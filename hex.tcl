namespace eval hex {}

proc hex::parse { file ctx {code ""}} {
	upvar $ctx this
	set this(entry) 0
	set upper 0
	set fd [open $file]
	while { -1!=[gets $fd line] } {
		if { ![regexp {^:(.*)} $line "" hexs] } continue
		set frame [binary format H* $hexs]
		set len [string length $frame]
		incr len -5
		binary scan $frame cu1Su1cu1a${len}a1 len address command data checksum
		#debug "0x%2x 0x%04x %s" $command $address [hexs $data]
		switch $command {
			0 {
				set this(address) [expr {$upper|$address}]
				set this(data) $data
				uplevel $code
			}
			4 {
				binary scan $data Su1 upper
				set upper [expr {$upper<<16}]
			}
			5 {
				binary scan $data I1 this(entry)
				debug "entry 0x%08x" $this(entry)
			}
			1 {
				# end
			}
			default {
				debug "bad code %d" $command
			}
		}
	}
	close $fd
	set this(entry)
}

if { $argv0==[info script] } {
	source [file dir [info script]]/debug.tcl
	
	hex::parse .build/test/test.hex this {
		debug "0x%08x %s" $this(address) [hexs $this(data)]
	}
	debug "0x%08x" $this(entry)
}
