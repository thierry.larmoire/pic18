
proc foreach_usb_serial { name code } {
	upvar $name ""
	foreach (tty) [lsort [glob -nocomplain /sys/bus/usb-serial/devices/*]] {
		set (usb) [file normalize [file dir $(tty)]/[file link $(tty)]/../..]
		foreach k { manufacturer product serial } {
			if { [file exists $(usb)/$k] } {
				set fd [open $(usb)/$k]
				gets $fd ($k)
				close $fd
			} {
				set ($k) ""
			}
		}
		uplevel $code
	}
}


if { $argv0==[info script] } {
	source debug.tcl
	foreach_usb_serial "" {
		set (dev) [file tail $(tty)]
		debug "\"%s\" \"%s\" \"%s\" \"%s\"" $(dev) $(manufacturer) $(product) $(serial)
	}
}
