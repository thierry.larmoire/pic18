#!/usr/bin/tclsh

proc debug { args } { puts stderr [eval format $args] }

proc hexs { data {max -1}} {
	set len [string length $data]
	if { $max!=-1 && $len>$max } {
		set len $max
	}
	binary scan $data H[expr {$len*2}] hexs
	set hexs
}

set stty [exec stty -g]
if { -1==[lsearch $argv --good] } {
	#debug good
	catch {
		lappend cmd exec >@stdout 2>@stderr <@stdin $argv0 --good
		eval $cmd $argv
	}
	exec stty $stty
	exit
}

proc cmd { name argv body } {
	lappend ::cmds $name
	proc $name $argv $body
}

cmd argv_help {} {
	debug "	-s <speed> ; -s 115200"
	debug "	-p <port> ; -p /dev/ttyUSB0"
	debug "	-e <esc char> ; \\<n> or <char>"
	debug "	-r <rts state> ; -r 1"
	debug "	-d <dtr state> ; -d 1"
	debug "	-X ; dump hex"
	debug "	-h ; this help"
}

cmd help {} {
	foreach cmd $::cmds {
		debug "%s %s" $cmd [info args $cmd]
	}
}

cmd rts { rts } {
	fconfigure $::fd -ttycontrol [list rts $rts]
}

cmd dtr { dtr } {
	fconfigure $::fd -ttycontrol [list dtr $dtr]
}

cmd speed { speed } {
	fconfigure $::fd -mode $speed,n,8,1
}

cmd esc { esc } {
	if { [scan $esc "\\%d" esc] } {
		set ::esc [format "%c" $esc]
	} {
		set ::esc $esc
	}
}

cmd hex { hex } {
	set ::hex $hex
}

set hex 0
set esc \3

foreach arg $argv {
	if { [info exists n] } {
		#debug %s $n
		set $n $arg
		unset n
		continue
	}
	switch -- $arg {
		--good {}
		-s { set n speed }
		-p { set n port }
		-e { set n esc }
		-r { set n rts }
		-d { set n dtr }
		-X { set hex 1 }
		-h { argv_help ; exit }
	}
}

exec stty -echo raw

set fd [open $port RDWR]
fconfigure $fd -blocking 0 -buffering none -translation binary

if { [info exist speed] } {
	fconfigure $fd -mode $speed,n,8,1
}

if { [info exist rts] } {
	rts $rts
}

if { [info exist dtr] } {
	dtr $dtr
}

esc $esc

#debug "esc is %s" [hexs $esc]

fconfigure stdin  -blocking 0 -translation binary
fconfigure stdout -buffering none -translation binary
fileevent stdin readable [list on_data << stdin  $fd]
fileevent $fd   readable [list on_data >> $fd stdout]


proc on_data { dir i o } {
	while { ""!=[set data [read $i]] } {
		if { $::esc==$data } {
			exec stty $::stty
			puts stdout ">>>>"
			set cmd ""
			fconfigure stdin -blocking 1 -translation auto
			while { -1!=[gets stdin line] } {
				if { $line=="quit" } break
				append cmd $line
				if { [info complete $cmd] } {
					set c [catch {
						set r [uplevel #0 $cmd]
					} r]
					puts stdout [format "%d %s" $c $r]
					puts stdout ">>>>"
					set cmd ""
				}
			}
			puts stdout "<<<<"
			exec stty -echo raw
			fconfigure stdin -blocking 0 -translation binary
			continue
		}
		if { $::hex } {
			debug "%s %s" $dir [hexs $data]
			if { $o!="stdout" } {
				puts -nonewline $o $data
			}
		} {
			puts -nonewline $o $data
		}
	}
	if { [eof $i] } {
		debug "eof %s" $i
		exit
	}
}

vwait forever


