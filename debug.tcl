proc bgerror { args } {
	puts stderr $args
	puts stderr $::errorInfo
}

proc debug { fmt args } {
	set string [eval [list format $fmt] $args]
	puts stderr $string
}

proc log { fmt args } {
	set string [eval [list format $fmt] $args]
	puts stderr [clock format [clock seconds] -format "--%Y/%m/%d-%H:%M:%S"]-$string
}

proc ifcatch { code {why_name {}} {catch {}} {ok {}} } {
	upvar $why_name why
	if { [catch { uplevel $code } why ] } {
		set then $catch
	} {
		set then $ok
	}
	if { $then!="" } {
		set r [catch { uplevel $then } result]
		return -code $r $result
	}
}

proc no { args } {
}

proc hexs { data {max -1}} {
	set len [string length $data]
	if { $max!=-1 && $len>$max } {
		set len $max
	}
	binary scan $data H[expr {$len*2}] hexs
	set hexs
}

proc '' { string } {
	append r \" [string map { \" \\\" \n \\n } $string] \"
}
