#!/usr/bin/tclsh

set root [file dir [info script]]
source $root/../debug.tcl
source $root/../hex.tcl
source $root/ftdi.tcl

namespace eval icsp {}

proc icsp::init { {ftdi_name "s:0x0403:0x6001:A50285BI"}} {
	variable ""
	
	set (bind) /sys/bus/usb/drivers/ftdi_sio/bind
	if { [file attributes $(bind) -group]!=$::env(USER) || [file attributes $(bind) -permissions]!=0220 } {
		debug "sudo chown :%s %s ; sudo chmod 0220 %s" $::env(USER) $(bind) $(bind)
		::exit
	}
	set (ftdi) [ftdi::open $ftdi_name]
	
	# FT232RL/FT245RL | Name | Signal
	# Pin Number      |      |
	#  1              | TXD  | D0        RB7/RX2/PGD with pull down
	#  5              | RXD  | D1        RB6/TX2/PGC with pull down
	#  3              | RTS# | D2
	# 11              | CTS# | D3
	#  2              | DTR# | D4        RE3/!MCLR   with pull down
	#  9              | DSR# | D5
	# 10              | DCD# | D6
	#  6              | RI#  | D7
	
	ftdi::defs $(ftdi) { pgd 0 pgc 1 mclr_ 4 }
	ftdi::dirs $(ftdi) { pgd i pgc i mclr_ i }
	
	ftdi::set_baudrate $(ftdi) 750000
	ftdi::set_latency_timer $(ftdi) 1

	#ftdi::chronogram $(ftdi) 1
	
	set b(mclr_) 0
	set b(pgd) 0
	set b(pgc) 0
	tx b
	ftdi::dirs $(ftdi) { pgd o pgc o mclr_ o }
	tx b
	after 1
	set b(mclr_) 1
	tx b
	after 1
	set b(mclr_) 0
	tx b
	after 1
	shift_key 32 0x4d434850
	after 1
	set b(mclr_) 1
	tx b
	after 1
	icsp::deviceId id
	debug "0x%02x 0x%02x" $id(1) $id(2)
	if { $id(1)==0x00 && $id(2)==0x00 } {
		debug "error"
		icsp::exit
		exit
	}
}

proc icsp::exit {} {
	variable ""
	set b(mclr_) 0
	set b(pgd) 0
	set b(pgc) 0
	tx b
	after 1
	ftdi::dirs $(ftdi) { pgd i pgc i mclr_ o }
	set b(mclr_) 1
	tx b
	ftdi::set_uart $(ftdi)
	ftdi::set_baudrate $(ftdi) 115200
	ftdi::close $(ftdi)
	
	foreach file [glob /sys/bus/usb/devices/*/manufacturer] {
		set fd [open $file]
		gets $fd manufacturer
		close $fd
		if { $manufacturer!="FTDI" } continue
		debug "%s %s" $file $manufacturer
		
		set fd [open $(bind) WRONLY]
		puts -nonewline $fd "[file tail [file dir $file]]:1.0"
		close $fd
	}
}

if 0 {
	rename ftdi::txs ftdi::txs_
	proc ftdi::txs { ftdi send } {
		debug "<< %s" [hexs $send]
		set recv [ftdi::txs_ $(ftdi) $send]
		debug ">> %s" [hexs $recv]
		set recv
	}
}

# based on 
# (1) DS30009977G  "PIC18FXXK80 FAMILY" "28/40/44/64-Pin, Enhanced Flash Microcontrollers with ECAN™ XLP Technology"
# (2) DS39972B     "PIC18FXXK80 FAMILY" "Flash Microcontroller Programming Specification"

proc icsp::tx { bs_name } {
	variable ""
	upvar $bs_name bs
	ftdi::push $(ftdi) send bs
	ftdi::push $(ftdi) send bs
	set recv [ftdi::txs $(ftdi) $send]
	ftdi::pull $(ftdi) recv bs
	ftdi::pull $(ftdi) recv bs
}

proc icsp::shift_key { len bits } {
	variable ""
	set b(mclr_) 0
	while { $len } {
		incr len -1
		set b(pgc) 0
		set b(pgd) [expr (($bits>>$len)&1)]
		ftdi::push $(ftdi) send b
		set b(pgc) 1
		ftdi::push $(ftdi) send b
	}
	set b(pgc) 0
	ftdi::push $(ftdi) send b
	set recv [ftdi::txs $(ftdi) $send]
}

proc icsp::shift { len txs {ins 0} } {
	variable ""
	upvar #0 $(ftdi) this
	array set ds $this(dirs)
	set b(mclr_) 1
	set send ""
	for { set i 0 } { $i<$len } { incr i } {
		set dir [lindex {o i} [expr (($ins>>$i)&1)]]
		if { $ds(pgd)!=$dir } {
			append recv [ftdi::txs $(ftdi) $send]
			set send ""
			set ds(pgd) $dir
			ftdi::dirs $(ftdi) [array get ds]
		}
		set b(pgc) 1
		set b(pgd) [expr (($txs>>$i)&1)]
		ftdi::push $(ftdi) send b
		set b(pgc) 0
		ftdi::push $(ftdi) send b
	}
	append recv [ftdi::txs $(ftdi) $send]
	set rxs 0
	for { set i 0 } { $i<$len } { incr i } {
		ftdi::pull $(ftdi) recv b
		ftdi::pull $(ftdi) recv b
		set rxs [expr {$rxs|($b(pgd)<<$i)}]
	}
	set rxs
}

proc icsp::shift_in { code data } {
	shift 20 [expr { ($data<<4)|$code }]
}

proc icsp::shift_out { code data_name } {
	upvar $data_name data
	set o [expr {$code | 0xffff0}]
	set i [shift 20 $o 0xffff0]
	set data [expr {($i>>12)&0xff}]
}

proc icsp::inst { inst } {
	shift_in 0x0 [expr $inst]
}

proc icsp::bsf { f bit {a 0} } {
	inst (8<<12)|($bit<<9)|($a<<8)|($f&0xff)
}

proc icsp::bcf { f bit {a 0} } {
	inst (9<<12)|($bit<<9)|($a<<8)|($f&0xff)
}

proc icsp::movlw { k } {
	inst (0x0e00|($k&0xff))
}

proc icsp::movwf { f {a 0} } {
	inst (0x6e00)|($a<<8)|($f&0xff)
}

proc icsp::movff { s d } {
	inst 0xc000|($s&0xff)
	inst 0xf000|($d&0xff)
}

proc icsp::nop {} {
	inst 0
}

proc icsp::nooop { duration } {
	variable ""
	upvar #0 $(ftdi) this
	shift 3 0
	set b(mclr_) 1
	set b(pgc) 1
	set b(pgd) 0
	ftdi::push $(ftdi) send b
	set recv [ftdi::txs $(ftdi) $send]
	ftdi::pull $(ftdi) recv b
	after $duration
	set b(pgc) 0
	ftdi::push $(ftdi) send b
	set recv [ftdi::txs $(ftdi) $send]
	ftdi::pull $(ftdi) recv b
	shift 16 0
}

proc icsp::tablat { args } {
	if { [llength $args] } {
		upvar [lindex $args 0] value
	}
	shift_out 0x2 value
	set value
}

proc icsp::tblrd   {} { shift_out 0x8 value }
proc icsp::tblrd++ {} { shift_out 0x9 value }
proc icsp::tblrd-- {} { shift_out 0xa value }
proc icsp::++tblrd {} { shift_out 0xb value }
proc icsp::tblwr   { lsb msb } { shift_in 0xc [expr { ($msb<<8)|$lsb }] }
proc icsp::tblwr++ { lsb msb } { shift_in 0xd [expr { ($msb<<8)|$lsb }] }
proc icsp::tblpg   { lsb msb } { shift_in 0xe [expr { ($msb<<8)|$lsb }] }
proc icsp::tblpg++ { lsb msb } { shift_in 0xf [expr { ($msb<<8)|$lsb }] }

for { set i 0 } { $i<0x20 } { incr i } {
	lappend unprintable [binary format c1 $i] .
}
for { set i 0x7f } { $i<0x100 } { incr i } {
	lappend unprintable [binary format c1 $i] .
}

proc icsp::tblptru { v } { movlw $v ; movwf $::tblptru }
proc icsp::tblptrh { v } { movlw $v ; movwf $::tblptrh }
proc icsp::tblptrl { v } { movlw $v ; movwf $::tblptrl }

proc icsp::tblptr { a } {
	tblptru [expr {$a>>16}]
	tblptrh [expr {$a>> 8}]
	tblptrl [expr {$a>> 0}]
}

proc icsp::dump { b l } {
	set e $b
	incr e $l
	tblptr $b
	for { set a $b } { $a<$e } { incr a } {
		set b [tblrd++]
		append hexs [format "%02x " $b]
		append asciis [string map $::unprintable [binary format c1 $b]]
		if { ($a&15)==15 } {
			puts stdout [format "%04x %s %s" [expr $a&~15] $hexs $asciis]
			unset hexs
			unset asciis
		}
	}
}

proc icsp::dump_flash { {len 512} } {
	dump 0 $len
}

proc icsp::config {} {
	dump 0x300000 16
}

proc icsp::test_led { {count 3600} } {
	set bit 5
	bcf $::trisb $bit
	set delay 500
	while { $count } {
		incr count -1
		bcf $::latb $bit
		after [expr {$delay-[clock millis]%$delay}]
		bsf $::latb $bit
		after [expr {$delay-[clock millis]%$delay}]
	}
}

proc icsp::test_wr_rd {} {
	foreach w { 0x81 0xaa 0x55 0xc3 0x3c 0x01 } {
		movlw $w
		movwf $::tablat
		nop
		tablat r
		debug "0x%02x 0x%02x" $w $r
	}
}

proc foreach_define { pic section_var name_var value_var code } {
	upvar $section_var section
	upvar $name_var name
	upvar $value_var value
	set fd [open [file dir [info script]]/../.dl/share/gputils/header/$pic.inc]
	while { -1!=[gets $fd line] } {
		#RXERRCNT         EQU  H'0E41'
		if 0 {
		} elseif { [regexp {^;----- (.*) -----------------------------------------------------$} $line . section] } {
			#debug "section %s" $section
		} elseif { [regexp {^$} $line] } {
			set section ""
		} elseif { [regexp {^(\S+)\s+EQU\s+H'([^'']+)'(|;.*)$} $line . name value] } {
			scan $value %x value
			set name [string tolower $name]
			#debug "%s 0x%04x" $name $value
			uplevel $code
		} elseif { [regexp {^(;.*|)$} $line] } {
		} {
			#debug %s $line
		}
	}
	close $fd
}
foreach_define p18f25k80 section name value {
	if { $section!="Register Files" } continue
	if { [info exists $name] } {
		debug "%s %s %s %s" $section $name [set $name] $value
	}
	#debug "%s 0x%04x" $name $value
	set $name $value
}

proc icsp::deviceId { ctx } {
	upvar $ctx ""
	tblptr 0x3ffffe
	set (1) [tblrd++]
	set (2) [tblrd++]
}

proc icsp::page_load { ctx } {
	upvar $ctx ""
	set (data) [list]
	if { $(erased) } {
		for { set i 0 } { $i<$(size) } { incr i } {
			lappend (data) 255
		}
	} {
		tblptr $(page)
		for { set i 0 } { $i<$(size) } { incr i } {
			lappend (data) [tblrd++]
		}
	}
	set (clear) 0
	set (diff) 0
}

proc icsp::page_save { ctx } {
	upvar $ctx ""
	if { $(debug)==1 } {
		debug "page_save 0x%02x 0x%02x 0x%08x %d" $(clear) $(diff) $(page) [llength $(data)]
	}
	if { $(debug)==2 } {
		puts -nonewline stderr [expr { !$(diff) ? "v" : $(clear) ? "W": "w" } ]
		flush stderr
	}
	if { !$(diff) } return
	tblptr $(page)
	if { $(clear) } {
		#debug "clear"
		movlw 0x96
		movwf $::eecon1
		nooop 1
		movlw 0x84
		movwf $::eecon1
	}
	set i 0
	foreach { lsb msb } [lrange $(data) 0 end-2] {
		#debug "wr %d 0x%02x 0x%02x" $i $lsb $msb
		tblwr++ $lsb $msb
		incr i
	}
	foreach { lsb msb } [lrange $(data) end-1 end] {
		#debug "pg %d 0x%02x 0x%02x" $i $lsb $msb
		tblpg $lsb $msb
		incr i
	}
	nooop 1
	
	set (clear) 0
	set (diff) 0
}

proc icsp::page_modify { ctx address byte } {
	upvar $ctx ""
	set (size) 0x40
	set page [expr {$address&~($(size)-1)} ]
	if { ![info exists (page)] } {
		set (page) $page
		page_load ""
	} {
		if { $(page)!=$page } {
			page_save ""
			set (page) $page
			page_load ""
		}
	}
	set i [expr {$address-$(page)}]
	set v [lindex $(data) $i]
	set (clear) [expr {$(clear) | (~$v&$byte)}]
	set (diff) [expr {$(diff) | ($v^$byte)}]
	set (data) [lreplace $(data) $i $i $byte]
}

proc icsp::prog_debug { } {
}

proc icsp::prog { hex {erase_all 1}} {
	lappend segments
	set address 0
	set data ""
	hex::parse $hex "" {
		set len [string length $(data)]
		if { $address+[string length $data]!=$(address) } {
			lappend segments $address $data
			set address $(address)
			set data ""
		}
		append data $(data)
		#debug "0x%08x %s" $(address) [hexs $(data)]
	}
	if { $data!="" } {
		lappend segments $address $data
	}
	set (erased) 0
	if { $erase_all } {
		erase 0x800005
		erase 0x800002
		erase 0x800104
		erase 0x800204
		erase 0x800404
		erase 0x800804
		incr (erased)
	}
	set (debug) 2
	set t(0) [clock millis]
	set done 0
	foreach { address data } $segments {
		debug "update 0x%06x %d" $address [string length $data]
		if { $address<0x200000 } {
			movlw 0x84
			movwf $::eecon1
			binary scan $data cu* bytes
			set a $address
			foreach byte $bytes {
				icsp::page_modify "" $a $byte
				incr a
			}
			icsp::page_save ""
			if { $(debug)==2 } {
				debug ""
			}
		} {
			movlw 0xc0
			movwf $::eecon1
			binary scan $data cu* bytes
			set a $address
			foreach byte $bytes {
				if { $(erased) } {
					set v 255
				} {
					tblptr $a
					set v [tblrd++]
				}
				if { $v^$byte } {
					debug "change config 0x%08x 0x%02x 0x%02x" $address $v $byte
					#puts -nonewline stderr "C"
					tblptr $a
					tblpg $byte $byte
					nooop 1
				} {
					#puts -nonewline stderr "c"
				}
				incr a
			}
		}
		incr done [string length $data]
	}
	set t(1) [clock millis]
	set t(d) [expr {$t(1)-$t(0)}]
	debug "%d bytes / %d millis = %.2lfKb/s" $done $t(d) [expr { 8.0*$done/$t(d) }]
}

proc icsp::erase { code } {
	tblptru 0x3c
	tblptrh 0x00
	for { set i 0 } { $i<3 } { incr i } {
		set b [expr {($code>> ($i*8))&0xff}]
		tblptrl [expr {0x04+$i}]
		tblwr $b $b
	}
	nooop 10
}


proc test {} {
	icsp::init
	#icsp::test_wr_rd
	#icsp::test_led 5
	#icsp::prog $::root/../.build/test/test.hex
	icsp::config
	set t(0) [clock millis]
	icsp::dump_flash
	set t(1) [clock millis]
	set t(d) [expr {$t(1)-$t(0)}]
	debug "%dms" $t(d)
	icsp::exit
}

proc erase {} {
	icsp::init
	icsp::erase 0x800005
	icsp::erase 0x800002
	icsp::erase 0x800104
	icsp::erase 0x800204
	icsp::erase 0x800404
	icsp::erase 0x800804
	icsp::exit
}

if { [info script]==$argv0 } {
	if { [info exists env(selection)] } {
		if { $env(selection)=="" } {
			test
		} {
			eval $env(selection)
		}
		exit
	}
	set tcl_interactive 1
	icsp::init
	source ~/.tclshrc
}
