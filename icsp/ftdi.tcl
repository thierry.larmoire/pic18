namespace eval ftdi {}

set lib [file dir [info script]]/../.build/ftdi.so
if { ![file exists $lib] } {
	file mkdir [file dir $lib]
	exec >@stdout 2>@stderr tclsh [file dir [info script]]/ftdi.cpp
}
load $lib

proc ftdi::init { string } {
	set ftdi [ftdi::open $string]
	upvar #0 $ftdi this
	list this(defs)
	list this(dirs)
	set ftdi
}

proc ftdi::set_uart { ftdi } {
	ftdi::set_bitmode $ftdi 0 0
}

proc ftdi::defs { ftdi defs } {
	upvar #0 $ftdi this

	set this(defs) $defs
}

proc ftdi::dirs { ftdi dirs } {
	upvar #0 $ftdi this

	set this(dirs) $dirs

	array set ds $this(dirs)
	set out 0
	foreach { signal b } $this(defs) {
		if { $ds($signal)=="o" } {
			incr out [expr {1<<$b} ]
		}
	}
	#debug "dirs %s" $this(dirs)
	#debug "dirs 0x%02x" $out
	ftdi::set_bitmode $ftdi $out
}

proc ftdi::dir { ftdi args } {
	upvar #0 $ftdi this
	array set ds $this(dirs)
	array set ds $args
	dirs $ftdi [array get ds]
}

proc ftdi::push { ftdi buf_name state_name } {
	upvar #0 $ftdi this
	
	upvar $buf_name buf
	upvar $state_name state

	array set ds $this(dirs)
	set byte 0
	foreach { signal b } $this(defs) {
		if { $ds($signal)!="o" } continue
		incr byte [expr { $state($signal)<<$b }]
	}
	if { ![info exists buf] } { set buf "" }
	#debug "push 0x%02x %d" $byte [string length $buf]
	append buf [binary format "cu1" $byte]
}

proc ftdi::pull { ftdi buf_name state_name } {
	upvar #0 $ftdi this
	
	upvar $buf_name buf
	upvar $state_name state

	array set ds $this(dirs)
	binary scan $buf "cu1 a*" byte buf
	foreach { signal b } $this(defs) {
		#if { $ds($signal)!="i" } continue
		set state($signal) [expr { ($byte>>$b)&1 }]
	}
}

proc ftdi::txsv { ftdi send } {
	upvar #0 $ftdi this
	
	set recv [ftdi::txs_ $ftdi $send]
	
	debug "send %3d %s" [string length $send] [hexs $send]
	debug "recv %3d %s" [string length $recv] [hexs $recv]
	
	binary scan $send cu* xs(send)
	binary scan $recv cu* xs(recv)
	
	array set ds $this(dirs)
	foreach { signal b } $this(defs) {
		foreach side { send recv } s { "  " "" } {
			set line ""
			foreach c $xs($side) {
				append line " " [expr {($c>>$b)&1}]
			}
			debug "%16s %s: %s%s" $signal $side $s $line
		}
	}
	set recv
}

proc ftdi::chronogram { ftdi on } {
	if { [llength [info command txsv]] && $on } {
debug "chrono 1"
		rename txs txs_
		rename txsv txs
	}
	if { [llength [info command txs_]] && !$on } {
debug "chrono 0"
		rename txs txsv
		rename txs_ txs
	}
}
