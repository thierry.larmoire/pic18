#if 0

set src [info script]
set path [file dir $src]
source $path/../debug.tcl
set out $path/../.build/[file root [file tail $src]].so
if { [catch {
	exec >@stdout 2>@stderr gcc -shared -fPIC -o $out $src -lftdi1
}] } {
	debug "try :"
	debug "	sudo apt-get install libftdi1-dev"
	debug "	sudo apt-get install tcl8.6-dev"
}

load $out

proc find_all {} {
	set count 0
	foreach dev [ftdi::find_all] {
		debug "--%s" $dev
		incr count
	}
	debug "found %d" $count
}

proc mark_it {} {
	set ftdi [ftdi::open "i:0x0403:0x6001"]
	ftdi::set_strings $ftdi FTDI {USB <-> Serial} swd
	ftdi::close $ftdi
}

proc eeprom_erase {} {
	set ftdi [ftdi::open "i:0x0403:0x6001"]
	ftdi::eeprom_erase $ftdi
	ftdi::close $ftdi
}

proc eeprom_dump {} {
	set ftdi [ftdi::open "i:0x0403:0x6001"]
	set content [ftdi::eeprom_rd $ftdi 128]
	debug "%d" [string length $content]
	debug "%s" [hexs $content]
	ftdi::close $ftdi
}

proc eeprom_restore {} {
	set ftdi [ftdi::open "i:0x0403:0x6001"]
	set hexs 0040030401600006802d08000002980aa21ec002231005000a0346005400440049001e0355005300420020003c002d003e002000530065007200690061006c000203020300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000cb10
	set content [binary format H* $hexs]
	debug "%d" [string length $content]
	ftdi::eeprom_wr $ftdi $content
	ftdi::close $ftdi
}

proc go {} {
	set ftdi [ftdi::open "i:0x0403:0x6001"]
	ftdi::set_bitmode $ftdi 0x00
	ftdi::set_baudrate $ftdi 115200
	ftdi::set_latency_timer $ftdi 1
	
	set got [ftdi::txs $ftdi "\xaa"]
	debug "%s" [hexs $got]
	
	ftdi::close $ftdi
}

if { [info exists env(selection)] && $env(selection)!="" } {
	eval $env(selection)
} {
	eval $argv
}
exit
#endif

/*
	# http://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT232R.pdf
	# http://www.ftdichip.com/Support/Documents/AppNotes/AN_232R-01_Bit_Bang_Mode_Available_For_FT232R_and_Ft245R.pdf

	FT232RL/FT245RL | Name | Signal
	Pin Number      |      |
	 1              | TXD  | D0
	 5              | RXD  | D1
	 3              | RTS# | D2
	11              | CTS# | D3
	 2              | DTR# | D4
	 9              | DSR# | D5
	10              | DCD# | D6
	 6              | RI#  | D7
*/

/*
echo 'ATTR{idVendor}=="0403", ATTR{idProduct}=="6001", MODE="0660", OWNER="1000"' | sudo tee /etc/udev/rules.d/50-ftdi.rules
sudo /etc/init.d/udev reload
*/

#include <stdio.h>
#include <stdlib.h>
#include <libftdi1/ftdi.h>
#include <time.h>
#include <tcl8.6/tcl.h>

#define MODULE ftdi
#define MODULE_NAME STRINGIFY(MODULE)
#define TclInit Ftdi_Init
#define TclExit Ftdi_Exit

#define TCL_CMD(func) \
	int proc_ ## func _ANSI_ARGS_((ClientData clientData,\
		Tcl_Interp *interp, int objc, struct Tcl_Obj *CONST objv[]))

#define TCL_PROCS \
	TCL_PROC(open) \
	TCL_PROC(close) \
	TCL_PROC(reset) \
	TCL_PROC(set_bitmode) \
	TCL_PROC(set_baudrate) \
	TCL_PROC(set_latency_timer) \
	TCL_PROC(txs) \
	TCL_PROC(find_all) \
	TCL_PROC(set_strings) \
	TCL_PROC(eeprom_erase) \
	TCL_PROC(eeprom_rd) \
	TCL_PROC(eeprom_wr) \
/**/

#define TCL_PROC(func) TCL_CMD(func);
TCL_PROCS
#undef TCL_PROC

extern "C" int TclInit(Tcl_Interp * interp);

int TclInit(Tcl_Interp * interp)
{
	#define TCL_PROC(func) Tcl_CreateObjCommand(interp, MODULE_NAME "::" #func, proc_ ## func,0/*clientdata*/,NULL/*deleteProc*/);
	TCL_PROCS
	#undef TCL_PROC
	return TCL_OK;
}

#define check(test,fmt,args...) \
	if(!(test)) \
	{ \
		char *error; \
		asprintf(&error,fmt,##args); \
		Tcl_SetResult(interp,error,(void (*)(char*))free); \
		return TCL_ERROR; \
	} \
/**/

TCL_CMD(open)
{
	int status=-1;
	struct ftdi_context *ftdi;
	const char *string;
	
	check(objc>=2,"%s <string>",Tcl_GetString(objv[0]));
	check(string=Tcl_GetString(objv[1]),"!string");
	
	ftdi=ftdi_new();
	check(ftdi,"!ftdi_new\n");
	
	status=ftdi_usb_open_string(ftdi, string);
	check(0==status,"!ftdi_usb_open %d %s\n", status, ftdi_get_error_string(ftdi));
	
	Tcl_SetLongObj(Tcl_GetObjResult(interp),(long)ftdi);
	
	return TCL_OK;
}

TCL_CMD(close)
{
	int status=-1;
	struct ftdi_context *ftdi;
	
	check(objc>=2,"%s <ftdi>",Tcl_GetString(objv[0]));
	check(TCL_OK==Tcl_GetLongFromObj(interp,objv[1],(long*)&ftdi),"!ftdi");
	
	status=ftdi_usb_close(ftdi);
	ftdi_free(ftdi);
	check(0==status,"unable to close ftdi device: %d %s\n", status, ftdi_get_error_string(ftdi));
	
	return TCL_OK;
}

TCL_CMD(reset)
{
	int status=-1;
	struct ftdi_context *ftdi;
	
	check(objc>=2,"%s <ftdi>",Tcl_GetString(objv[0]));
	check(TCL_OK==Tcl_GetLongFromObj(interp,objv[1],(long*)&ftdi),"!ftdi");
	
	status=ftdi_usb_reset(ftdi);
	check(0==status,"unable to reset ftdi device: %d %s\n", status, ftdi_get_error_string(ftdi));
	
	return TCL_OK;
}

TCL_CMD(set_bitmode)
{
	int status=-1;
	struct ftdi_context *ftdi;
	int bitoption;
	int bitmode=BITMODE_SYNCBB;
	
	check(objc>=3,"%s <ftdi> <bitoption> <bitmode(BITMODE_SYNCBB)>",Tcl_GetString(objv[0]));
	check(TCL_OK==Tcl_GetLongFromObj(interp,objv[1],(long*)&ftdi),"!ftdi");
	check(TCL_OK==Tcl_GetIntFromObj(interp,objv[2],&bitoption),"!bitoption");
	if(objc>=4)
	check(TCL_OK==Tcl_GetIntFromObj(interp,objv[3],&bitmode),"!bitmode");
	
	//fprintf(stderr,"bitmode 0x%02x 0x%02x\n",bitoption,bitmode);
	status=ftdi_set_bitmode(ftdi,bitoption,bitmode);
	check(0==status,"!ftdi_set_bitmode %d %s\n", status, ftdi_get_error_string(ftdi));
	
	return TCL_OK;
}

TCL_CMD(set_baudrate)
{
	int status=-1;
	struct ftdi_context *ftdi;
	int baudrate;
	
	check(objc>=3,"%s <ftdi> <baudrate>",Tcl_GetString(objv[0]));
	check(TCL_OK==Tcl_GetLongFromObj(interp,objv[1],(long*)&ftdi),"!ftdi");
	check(TCL_OK==Tcl_GetIntFromObj(interp,objv[2],&baudrate),"!baudrate");
	
	status=ftdi_set_baudrate(ftdi,baudrate);
	check(0==status,"!ftdi_set_baudrate %d %s\n", status, ftdi_get_error_string(ftdi));
	
	return TCL_OK;
}

TCL_CMD(set_latency_timer)
{
	int status=-1;
	struct ftdi_context *ftdi;
	int latency_timer;
	
	check(objc>=3,"%s <ftdi> <baudrate>",Tcl_GetString(objv[0]));
	check(TCL_OK==Tcl_GetLongFromObj(interp,objv[1],(long*)&ftdi),"!ftdi");
	check(TCL_OK==Tcl_GetIntFromObj(interp,objv[2],&latency_timer),"!latency_timer");
	
	status=ftdi_set_latency_timer(ftdi,latency_timer);
	check(0==status,"!ftdi_set_latency_timer %d %s\n", status, ftdi_get_error_string(ftdi));
	
	return TCL_OK;
}

struct Nano
{
	uint64_t v;
	inline Nano& now()
	{
		timespec ts;
		clock_gettime(CLOCK_REALTIME,&ts);
		v=ts.tv_sec*1000000000+ts.tv_nsec;
		return *this;
	}
	inline operator long long()
	{
		return v;
	}
};

Nano operator-(const Nano& l,const Nano& r)
{
	Nano n;
	n.v=l.v-r.v;
	return n;
}

TCL_CMD(txs)
{
	//Nano ns[2];
	int status=-1;
	struct ftdi_context *ftdi;
	struct Buffer
	{
		unsigned char *ptr;
		int len;
	}send,recv;
	int done;
	
	check(objc>=3,"%s <ftdi> <send buffer>",Tcl_GetString(objv[0]));
	check(TCL_OK==Tcl_GetLongFromObj(interp,objv[1],(long*)&ftdi),"!ftdi");
	check(send.ptr=Tcl_GetByteArrayFromObj(objv[2], &send.len),"!send");
	Tcl_Obj *r=Tcl_GetObjResult(interp);
	Tcl_SetByteArrayLength(r,send.len);
	check(recv.ptr=Tcl_GetByteArrayFromObj(r, &recv.len),"!recv");
	if(send.len==0)
		return TCL_OK;
	//ns[0].now();
	check(send.len==(done=ftdi_write_data(ftdi, send.ptr, send.len)), "!write %d!=%d", done, send.len);
	//ns[1].now();
	//fprintf(stderr,"wr %3d %10ld\n",length,(long)(ns[1]-ns[0]));
	//ns[0].now();
	check(recv.len==(done=ftdi_read_data (ftdi, recv.ptr, recv.len)), "!read  %d!=%d", done,recv.len);
	//ns[1].now();
	//fprintf(stderr,"rd %3d %10ld\n",length,(long)(ns[1]-ns[0]));
	
	return TCL_OK;
}

TCL_CMD(find_all)
{
	struct ftdi_context *ftdi;
	struct ftdi_device_list *list=NULL;
	Tcl_Obj *all=Tcl_NewListObj(0, 0);
	
	ftdi=ftdi_new();
	check(ftdi,"!ftdi_new\n");
	
	int count=ftdi_usb_find_all(ftdi,&list,0,0);
	for(ftdi_device_list *curr=list;curr;curr=curr->next)
	{
		char manufacturer[128]={'\0'};
		char description[128]={'\0'};
		char serial[128]={'\0'};
		ftdi_usb_get_strings(ftdi,curr->dev,manufacturer,sizeof(manufacturer),description,sizeof(description),serial,sizeof(serial));
		Tcl_Obj *ss[3];
		ss[0]=Tcl_NewStringObj(manufacturer,-1);
		ss[1]=Tcl_NewStringObj(description,-1);
		ss[2]=Tcl_NewStringObj(serial,-1);
		Tcl_Obj *one=Tcl_NewListObj(3, ss);
		Tcl_ListObjAppendElement(interp,all,one);
	}
	
	Tcl_SetObjResult(interp,all);
	
	ftdi_list_free(&list);
	ftdi_free(ftdi);
	
	return TCL_OK;
}

TCL_CMD(set_strings)
{
	int status=-1;
	struct ftdi_context *ftdi;
	char *manufacturer=NULL;
	char *product=NULL;
	char *serial=NULL;
	int length=0,done;
	
	check(objc>=2,"%s <ftdi> <manufacturer> <product> <serial>",Tcl_GetString(objv[0]));
	check(TCL_OK==Tcl_GetLongFromObj(interp,objv[1],(long*)&ftdi),"!ftdi");
	if(objc>=3)
	check(manufacturer=Tcl_GetString(objv[2]),"!manufacturer");
	if(objc>=4)
	check(product=Tcl_GetString(objv[3]),"!product");
	if(objc>=5)
	check(serial=Tcl_GetString(objv[4]),"!serial");
	
	//check(0<=ftdi_read_eeprom(ftdi),"!read");
	//check(0<=ftdi_eeprom_decode(ftdi,0),"!decode");
	//check(0<=ftdi_eeprom_set_strings(ftdi,manufacturer,product,serial),"!set");
	check(0<=ftdi_eeprom_initdefaults(ftdi,manufacturer,product,serial),"!default");
	check(0<=ftdi_eeprom_build(ftdi),"!build");
	check(0<=ftdi_write_eeprom(ftdi),"!write");
	
	return TCL_OK;
}

TCL_CMD(eeprom_erase)
{
	int status=-1;
	struct ftdi_context *ftdi;
	
	check(objc>=2,"%s <ftdi>",Tcl_GetString(objv[0]));
	check(TCL_OK==Tcl_GetLongFromObj(interp,objv[1],(long*)&ftdi),"!ftdi");
	
	check(0==(status=ftdi_erase_eeprom(ftdi)),"error %d\n",status);
	
	return TCL_OK;
}

TCL_CMD(eeprom_rd)
{
	int status=-1;
	struct ftdi_context *ftdi;
	int length;
	
	check(objc>=3,"%s <ftdi> <length>",Tcl_GetString(objv[0]));
	check(TCL_OK==Tcl_GetLongFromObj(interp,objv[1],(long*)&ftdi),"!ftdi");
	check(TCL_OK==Tcl_GetIntFromObj(interp,objv[2],&length),"!length");
	unsigned char buffer[length];
	check(0<=(status=ftdi_read_eeprom(ftdi)),"!read %d",status);
	check(0<=(status=ftdi_get_eeprom_buf(ftdi,buffer,length)), "!get %d", status);
	Tcl_SetByteArrayObj(Tcl_GetObjResult(interp), buffer, length);
	
	return TCL_OK;
}

TCL_CMD(eeprom_wr)
{
	int status=-1;
	struct ftdi_context *ftdi;
	unsigned char *buffer;
	int length=0;
	
	check(objc>=3,"%s <ftdi> <buffer>",Tcl_GetString(objv[0]));
	check(TCL_OK==Tcl_GetLongFromObj(interp,objv[1],(long*)&ftdi),"!ftdi");
	check(buffer=Tcl_GetByteArrayFromObj(objv[2], &length),"!buffer");
	check(0<=ftdi_eeprom_build(ftdi),"!build");
	check(0<=(status=ftdi_set_eeprom_buf(ftdi,buffer,length)), "!set %d", status);
	check(0<=(status=ftdi_write_eeprom(ftdi)),"!write %d",status);
	
	return TCL_OK;
}

