#define __18F25K80
#pragma config XINST=OFF
#include <pic18f25k80.h>

/*
	(1) /data/documentations/electronics/microControleur/pic/PIC18F66K80 FAMILY Enhanced Flash MCU with ECAN XLP Technology 30009977G.pdf
*/

void blink_led()
{
	TRISBbits.TRISB5=0;
	while(1)
	{
		for(long i=0;i<100000;i++)
			;
		LATBbits.LATB5=!LATBbits.LATB5;
	}
}

void serial_echo()
{
	OSCCON=0x60; // internal 8MHz
	OSCTUNEbits.PLLEN=1; // pll *4 =>32MHz
	
	TRISBbits.TRISB5=0;// pin26 led
	TRISBbits.TRISB6=0;// pin27 TX2
	TRISBbits.TRISB7=1;// pin28 RX2
	
	BAUDCON2bits.BRG16=1;
	TXSTA2bits.BRGH=1;
/* (1) §22.2
osc=$((32*1000*1000))
expected=115200
divisor=$(($osc/$expected/4-1))
echo $divisor
for d in -1 0 1 ; do b=$(($osc/(4*($divisor+d+1)) -$expected )) ; printf "%d => %d%+-6d %+6dppm\n" $(( $divisor + $d )) $expected $b $(( $b*1000000/$expected )) ; done

*/
	const int div=68;
	SPBRGH2=div>>8;
	SPBRG2=div&0xff;
	
	RCSTA2bits.SPEN=1;
	RCSTA2bits.CREN=1;
	TXSTA2bits.TXEN=1;
	while(1)
	{
		unsigned char c;
		LATBbits.LATB5=!LATBbits.LATB5;
		#if 1
		while(!PIR3bits.RC2IF)
			;
		c=RCREG2;
		#else
		c='U';
		#endif
		while(!PIR3bits.TX2IF)
			;
		TXREG2=c;
	}
}

void main()
{
	//blink_led();
	serial_echo();
}
