#!/usr/bin/tclsh

cd [file dir [info script]]
source ../makefile.tcl

lappend prj(srcs) test.c

compile prj

if { [lindex $argv 0]=="go" } {
	source ../icsp/icsp.tcl
	icsp::init
	icsp::prog $prj(hex)
	#icsp::dump_flash
	icsp::exit
}
