#define __18F25K80
#pragma config XINST=OFF
#include <pic18f25k80.h>

/*

         30660
        +-----+
  TxD 1-|     |-8 S
  GND 2-|     |-7 CANH
  VCC 3-|     |-6 CANL
  RxD 4-|     |-5 Vref
        +-----+


== CiA Draft Recommendation Proposal 303-1 ==
Pin Signal     Description
1   -          Reserved
2   CAN_L      CAN_L bus line (dominant low)
3   CAN_GND    CAN Ground
4   -          Reserved
5   (CAN_SHLD) Optional CAN Shield
6   (GND)      Optional Ground
7   CAN_H      CAN_H bus line (dominant high)
8   -          Reserved
9   (CAN_V+)   Optional CAN external positive supply (dedicated for supply of
               transceiver and opto-couplers, if galvanic isolation of the bus node applies)

*/


typedef unsigned  char U8;
typedef   signed  char S8;
typedef unsigned short U16;
typedef   signed short S16;
typedef unsigned  long U32;
typedef   signed  long S32;

#define STRUCT(name)\
struct name ## _; \
typedef struct name ## _ name; \
struct name ## _ \
/**/

/*
	(1) /data/documentations/electronics/microControleur/pic/PIC18F66K80 FAMILY Enhanced Flash MCU with ECAN XLP Technology 30009977G.pdf
	(2) linux-5.4.0/include/uapi/linux/can.h linux-5.4.0/drivers/net/can/slcan.c
	(3) /data/documentations/electronics/microControleur/pic/00878a.pdf
	(4) /data/documentations/electronics/microControleur/pic/an878.zip
*/

void blink_led()
{
	TRISBbits.TRISB5=0;
	while(1)
	{
		for(long i=0;i<100000;i++)
			;
		LATBbits.LATB5=!LATBbits.LATB5;
	}
}

U8 dehex(U8 c)
{
	return (c&0x40) ? (c|0x20)-('a'-10) : c-'0';
}

U8 enhex(U8 c)
{
	return c+ ((c<10) ? '0' : (-10+'A'));
}

STRUCT(Serial_data)
{
	int len;
	int idx;
};

STRUCT(Serial)
{
	Serial_data rx;
	Serial_data tx;
};

U8 serial_rx_data[256];
U8 serial_tx_data[256];

Serial serial[1];

void serial_init()
{
	TRISBbits.TRISB5=0;// pin26 led
	TRISBbits.TRISB6=0;// pin27 TX2
	TRISBbits.TRISB7=1;// pin28 RX2
	
	BAUDCON2bits.BRG16=1;
	TXSTA2bits.BRGH=1;
/* (1) §22.2
osc=$((64*1000*1000))
expected=115200
divisor=$(($osc/$expected/4-1))
echo $divisor
for d in -1 0 1 ; do b=$(($osc/(4*($divisor+d+1)) -$expected )) ; printf "%d => %d%+-6d %+6dppm\n" $(( $divisor + $d )) $expected $b $(( $b*1000000/$expected )) ; done
*/
	const int div=138;
	SPBRGH2=div>>8;
	SPBRG2=div&0xff;
	
	RCSTA2bits.SPEN=1;
	RCSTA2bits.CREN=1;
	TXSTA2bits.TXEN=1;

	serial->rx.len=0;
	serial->rx.idx=0;
	serial->tx.len=0;
	serial->tx.idx=0;
}

int serial_demux(U8 c)
{
	if(c=='\r' || serial->rx.len>=sizeof(serial_rx_data))
		return 1;
	serial_rx_data[serial->rx.len++]=c;
	return 0;
}
#define tblptr(s) \
do \
{ \
	TBLPTRU=(U8)(((int)s)>>16); \
	TBLPTRH=(U8)(((int)s)>> 8); \
	TBLPTRL=(U8)(((int)s)    ); \
} \
while(0) \
/**/

#define tblrdpp() __asm__( "tblrd*+" )
#define tblwrpp() __asm__( "tblwr*+" )

void debug(const char*s)
{
	tblptr(s);
	while(1)
	{
		tblrdpp();
		U8 c=TABLAT;
		if(!c)
			break;
		while(!PIR3bits.TX2IF);	TXREG2=c;
		if(c=='\n')
		{
			while(!PIR3bits.TX2IF); TXREG2='\r';
		}
	}
}

void debugU8(U8 v)
{
	while(!PIR3bits.TX2IF);	TXREG2=enhex(v>>4);
	while(!PIR3bits.TX2IF);	TXREG2=enhex(v&15);
}

void debugU32(U32 v)
{
	debugU8(v>>24);
	debugU8(v>>16);
	debugU8(v>> 8);
	debugU8(v>> 0);
}

STRUCT(Can)
{
	U32 id;
	U8 dt_len:4;
	U8 eff:1;
	U8 rtr:1;
	U8 err:1;
	U8 use:1;
	U8 dt[8];
};

Can rx[1],tx[1];

#define CAN_MODE_MASK     0xe0
#define CAN_MODE_NORMAL   0x00
#define CAN_MODE_DISABLE  0x20
#define CAN_MODE_LOOPBACK 0x40
#define CAN_MODE_LISTEN   0x60
#define CAN_MODE_CONFIG   0x80

void can_init()
{
	TRISBbits.TRISB2=0;// pin23 CANTX
	TRISBbits.TRISB3=1;// pin24 CANRX
	
	rx->use=0;
	tx->use=0;
}

void can_open()
{
	CANCON= (CANCON&~CAN_MODE_MASK)|CAN_MODE_NORMAL;
	while((CANSTAT & CAN_MODE_MASK)!=CAN_MODE_NORMAL)
		;
}

void can_listen()
{
	CANCON= (CANCON&~CAN_MODE_MASK)|CAN_MODE_LISTEN;
	while((CANSTAT & CAN_MODE_MASK)!=CAN_MODE_LISTEN)
		;
}

void can_close()
{
	CANCON= (CANCON&~CAN_MODE_MASK)|CAN_MODE_CONFIG;
	while((CANSTAT & CAN_MODE_MASK)!=CAN_MODE_CONFIG)
		;
}

void can_speed(int megabit_div)
{
	// (1) §27.9.2
	#define CAN_FOSC 64
	#define CAN_FQUANTA 16
	#define   CAN_SEG_SYNC   1 // [1..1]
	#define   CAN_SEG_PROP   4 // [1..8]
	#define   CAN_SEG_PHASE1 6 // [1..8]
	#define   CAN_SEG_PHASE2 5 // [1..8]
	#define CAN_SJW 2 // [1..4]
	
	BRGCON1bits.BRP=((CAN_FOSC/CAN_FQUANTA)/2*megabit_div-1);
	BRGCON1bits.SJW=(CAN_SJW-1);
	BRGCON2bits.SEG2PHTS=1;
	BRGCON2bits.SAM=1;
	BRGCON2bits.PRSEG=CAN_SEG_PROP-1;
	BRGCON2bits.SEG1PH=CAN_SEG_PHASE1-1;
	BRGCON3bits.SEG2PH=CAN_SEG_PHASE2-1;
	BRGCON3bits.WAKDIS=1;
	BRGCON3bits.WAKFIL=0;
	
	CIOCONbits.CLKSEL=0;
	CIOCONbits.ENDRHI=1;
	
	ECANCONbits.MDSEL=1;
	RXB0CONbits.RXM1=0;
	RXB1CONbits.RXM1=0;
}

void can_demux()
{
#define can tx
#define getc(c) \
	if(serial->rx.idx>=serial->rx.len) \
		return; \
	*c=serial_rx_data[serial->rx.idx++] \
/**/
	
	U8 c,l;
	getc(&c);
	can->eff= c=='R'||c=='T';
	can->rtr= c=='r'||c=='R';
	l= ((can->eff?29:11)-1)/4+1;
	can->id=0;
	while(l--)
	{
		getc(&c);
		can->id<<=4;
		can->id|= dehex(c);
	}
	getc(&c);
	can->dt_len=dehex(c);
	if(!can->rtr)
	for(l=0;l<can->dt_len;l++)
	{
		getc(&c);
		can->dt[l]= dehex(c)<<4;
		getc(&c);
		can->dt[l]|= dehex(c);
	}
	can->use=1;
#undef getc
#undef can
}

void can_enmux()
{
#define can rx
#define putc(c) \
	if(serial->tx.len>=sizeof(serial_tx_data)) \
		return; \
	serial_tx_data[serial->tx.len++]=c \
/**/
	serial->tx.idx=0;
	serial->tx.len=0;
	U8 c=can->rtr?'r':'t';
	if(can->eff)
		c^=('t'^'T');
	putc(c);
	U8 l= ((can->eff?29:11)-1)/4+1;
	while(l--)
	{
		putc(enhex((can->id>>(l*4))&0xf));
	}
	putc(enhex(can->dt_len));
	if(!can->rtr)
	for(l=0;l<can->dt_len;l++)
	{
		putc(enhex(can->dt[l]>>4 ));
		putc(enhex(can->dt[l]&0xf));
	}
	putc('\r');
	putc('\n');
#undef putc
#undef can
}

void can_send()
{
#define can tx
	debug("I txb0\n");
	TXB0DLCbits.DLC=can->dt_len;
	TXB0DLCbits.TXRTR=can->rtr;
	if(!can->rtr)
	{
		U8 l=can->dt_len;
		U8 *s=can->dt;
		U8 *d=&TXB0D0;
		TXB0DLC=can->dt_len&0xf;
		//debug("D ");
		while(l--)
		{
			//debugU8(*s);
			*d++=*s++;
		}
		//debug("\n");
	}
	if(can->eff)
	{
		TXB0EIDL=can->id>>0;
		TXB0EIDH=can->id>>8;
		TXB0SIDL=(((U8)(can->id>>18)&7)<<5)|_TXB0SIDL_EXIDE| ((can->id>>16)&3);
		TXB0SIDH=can->id>>21;
	}
	else
	{
		TXB0SIDL=can->id<<5;
		TXB0SIDH=can->id>>3;
	}
	TXB0CONbits.TXREQ=1;
#undef can
}

void can_recv()
{
#define can rx
	debug("I rxb0\n");
	can->eff=RXB0SIDLbits.EXID;
	can->rtr=RXB0DLCbits.RXRTR;
	can->dt_len=RXB0DLCbits.DLC;
	
	if(can->eff)
	{
		can->id= 0
			| (((U32)RXB0SIDH        )<<21)
			| (((U32)RXB0SIDLbits.SID)<<18)
			| (((U32)(RXB0SIDL&3))<<16)
			| (((U32)RXB0EIDH        )<< 8)
			| (((U32)RXB0EIDL        )<< 0)
		;
	}
	else
	{
		can->id= (RXB0SIDH<<3)|RXB0SIDLbits.SID;
	}
	
	#if 0
	#define d(r) debug("D " #r " ");debugU8(r);debug("\n");
	d(RXB0SIDH)
	d(RXB0SIDL)
	d(RXB0EIDH)
	d(RXB0EIDL)
	d(RXB0DLC)
	#undef d
	#endif
	
	if(!can->rtr)
	{
		U8 l=can->dt_len;
		U8 *s=&RXB0D0;
		U8 *d=can->dt;
		//debug("D ");
		while(l--)
		{
			//debugU8(*s);
			*d++=*s++;
		}
		//debug("\n");
	}
	RXB0CONbits.RXFUL=0;
#undef can
}

void handle_serial_cmd()
{
	switch(serial_rx_data[0])
	{
	case 'F':
	{
		#define d(r) debug("F " #r " ");debugU8(r);debug("\n");
		d(CANCON  )
		d(TXB0CON )
		d(TXB0DLC )
		d(TXB0SIDL)
		d(TXB0SIDH)
		d(TXB0EIDL)
		d(TXB0EIDH)
		#undef d
		
		#if 0
		#define d(r) debug("F " #r " ");debugU8(rx->r);debug("\n");
		d(dt_len)
		d(dt[0])
		d(dt[1])
		d(dt[2])
		d(dt[3])
		d(dt[4])
		d(dt[5])
		d(dt[6])
		d(dt[7])
		#undef d
		#define d(r) debug("F " #r " ");debugU32(rx->r);debug("\n");
		d(id)
		#undef d
		#endif
	}
	break;
	case 'O':
	{
		can_open();
	}
	break;
	case 'L':
	{
		can_listen();
	}
	break;
	case 'C':
	{
		can_close();
		//CANCONbits.ABAT=1;
		//TXB0CONbits.TXREQ=0;
		//LATBbits.LATB5=0;
	}
	break;
	case 't':
	case 'T':
	case 'r':
	case 'R':
	{
		if(!tx->use)
		{
			can_demux();
			if(!tx->use)
			{
				debug("E format\n");
			}
		}
		else
		{
			debug("E busy\n");
		}
	}
	break;
	default:
	{
		debug("E bad command\n");
	}
	}
}

void main()
{
	OSCCON=0x70; // internal 8MHz
	OSCTUNEbits.PLLEN=1; // pll *4 =>32MHz
	TRISBbits.TRISB5=0;
	
	serial_init();
	can_init();
	can_close();
	can_speed(1);
	can_open();
	
	while(1)
	{
		if(PIR3bits.RC2IF)
		{
			U8 c=RCREG2;
			while(!PIR3bits.TX2IF);	TXREG2=c;
			if(c=='\r')
			{
				while(!PIR3bits.TX2IF);	TXREG2='\n';
				//debug("I hello\n");
				//LATBbits.LATB5=!LATBbits.LATB5;
				if(serial->rx.len<=sizeof(serial_rx_data))
				{
					handle_serial_cmd();
				}
				else
				{
					debug("E serial full\n");
				}
				
				serial->rx.len=0;
				serial->rx.idx=0;
			}
			else
			{
				if(serial->rx.len<sizeof(serial_rx_data))
					serial_rx_data[serial->rx.len]=c;
				serial->rx.len++;
			}
		}
		if(PIR3bits.TX2IF && serial->tx.idx<serial->tx.len)
			TXREG2=serial_tx_data[serial->tx.idx++];
		if(RXB0CONbits.RXFUL)
		{
			can_recv();
			can_enmux();
		}
		if(RXB1CONbits.RXFUL)
		{
			debug("I rxb1\n");
			U8 dlc=RXB1DLC;
			
			RXB1CONbits.RXFUL=0;
		}
		#if 0
		#define change(r) \
		{ \
			static U8 was; \
			if(r!=was) \
			{ \
				was=r; \
				debug("I " #r " ");debugU8(r);debug("\n"); \
			} \
		} \
		/**/
		change(TXB0CON)
		change(COMSTAT)
		#endif
		if(tx->use && !TXB0CONbits.TXREQ)
		{
			can_send();
			tx->use=0;
		}
	}
}
