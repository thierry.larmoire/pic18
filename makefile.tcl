
set root [file normalize [file dir [info script]]]
set build $root/.build
set dl    $root/.dl

set env(PATH) $dl/bin:$env(PATH)

source $root/debug.tcl

proc touch { file } {
	if { ![file exists $file] } {
		close [open $file {CREAT WRONLY}]
	} {
		file mtime $file [clock seconds]
	}
}

proc in_build { path } {
	set path [file normalize $path]
	set path [string range $path [string length $::root] end]
	set path $::build$path
}

proc stat { stat_name } {
	upvar $stat_name stat
	set stat(extension) [file extension $stat(name)]
	ifcatch {
		file lstat $stat(name) stat
	} pb {
		set stat(mtime) 0
	}
}

proc depend { dst srcs code } {
	upvar #0 $dst this
	set this(srcs) $srcs
	set this(code) $code
	#debug "depend %s from %s" $dst $this(srcs)
}

proc make_start { dst } {
	upvar #0 $dst this
	set this(todo) 0
	set this(done) 0
	#debug "make %s" $dst
	if { ![info exists this(srcs)] } return
	#debug "make %s from %s" $dst $this(srcs)
	set m 0
	foreach src $this(srcs) {
		make_start $src
	}
	foreach src $this(srcs) {
		set t [make_wait $src]
		if { $t>=$m } { set m $t }
	}
	set d(name) $dst
	stat d
	if { 0==$d(mtime) } {
		file mkdir [file dir $d(name)]
	}
	if { $m>$d(mtime) } {
		uplevel $this(code)
	}
}

proc make_wait { dst } {
	if { [info exists ::$dst] } {
		upvar #0 $dst this
		while { $this(done)<$this(todo) } {
			vwait ::${dst}(done)
		}
	}
	#debug "done %s" $dst
	file mtime $dst
}

proc make { dst } {
	make_start $dst
	make_wait $dst
}

proc job { dst cmd } {
	upvar #0 $dst this
	incr this(todo)
	debug "%s {%s}" $dst $cmd
	debug "%s" $dst
	set this(fd) [open [concat [list | 2>@stderr] $cmd]]
	fconfigure $this(fd) -blocking 0
	fileevent $this(fd) readable [list on_job $dst]
}

proc on_job { dst } {
	upvar #0 $dst this
	while { -1!=[gets $this(fd) line] } {
		debug "%s" $line
	}
	if { [eof $this(fd)] } {
		#debug "done %s" $dst
		close $this(fd)
		incr this(done)
	}
}

proc c_deps { file } {
	upvar #0 deps_$file deps
	
	if { ![file exists $file] } {
		#debug "!%s" $file
		return [list]
	}
	if { [info exists deps] } {
		set method get
	} {
		lappend deps
		set d [in_build $file].d
		if { ![file exists $d] || [file mtime $d]<[file mtime $file] } {
			set method create
			set fd [open $file]
			while { -1!=[gets $fd line] } {
				if { ![regexp {.include "(.*)"} $line . include] } continue
				regsub {[^/]*$} $file $include include
				while { [regsub -all {/[^/]+/\.\./} $include "/" include] } {
					#debug ".. %s" $include
				}
				lappend deps $include
			}
			close $fd
			file mkdir [file dir $d]
			set fd [open $d {WRONLY CREAT TRUNC}]
			puts $fd $deps
			close $fd
		} {
			set method read
			set fd [open $d]
			gets $fd deps
			close $fd
		}
	}
	#debug "dep %-20s %s" $method $file
	set deps
}

proc compile { prj_name } {
	upvar $prj_name prj
	foreach { k v } {
		optimize 3
		all 0
		cflags ""
		ldflags ""
	} {
		if { ![info exists prj($k)] } {
			set prj($k) $v
		}
	}
	
	foreach { auto code } {
		name { file root [lindex $prj(srcs) 0] }
		out  { in_build $prj(name) }
		bin  { format "%s.bin" $prj(out) }
		hex  { format "%s.hex" $prj(out) }
		ld   { format "%s.ld"  $prj(out) }
		map  { format "%s.map" $prj(out) }
		asm  { format "%s.asm" $prj(out) }
		s    { format "%s.s"   $prj(out) }
	} {
		if { [info exists prj($auto)] } continue
		set prj($auto) [eval $code]
	}
	
	set srcs $prj(srcs)
	while { [llength $srcs] } {
		set src(name) [lindex $srcs 0]
		set srcs [lrange $srcs 1 end]
		stat src
		set dst(name) [in_build [file root $src(name)]]
		switch $src(extension) {
			".tcl" {
				depend $dst(name) [list $src(name)] [list job $dst(name) [list tclsh $src(name)]]
			}
			".png" - ".xpm" {
				append dst(name) ".cpp"
				depend $dst(name) [list $src(name)] [list job $dst(name) [list $::root/img_to_src.tcl $src(name)]]
				lappend srcs $dst(name)
			}
			".c" {
				append dst(name) ".o"
				
				lappend deps $src(name)
				lappend todos $src(name)
				while { [llength $todos] } {
					set todo [lindex $todos end]
					set todos [lrange $todos 0 end-1]
					foreach dep [c_deps $todo] {
						set dep [file normalize $dep]
						if { -1==[lsearch $deps $dep] } {
							#debug "%s" $dep
							lappend deps $dep
							lappend todos $dep
						}
					}
				}
				depend $dst(name) $deps [list job $dst(name) [concat [list sdcc --use-non-free -mpic16 -p18f25k80] $prj(cflags) [list  -c $src(name) -o $dst(name) ]]]
				unset deps
				lappend objs $dst(name)
			}
		}
		stat dst
		if { $prj(all) && $dst(mtime) } {
			file mtime $dst(name) 0
		}
		#debug "%d %d" $dst(mtime) $src(mtime)
	}
	
	lappend cmd sdcc --use-non-free -mpic16 -p18f25k80
	lappend cmd -o $prj(hex)
	set deps $objs
	#lappend deps [uplevel { info script }]
	#lappend deps $prj(ld)
	depend $prj(hex) $deps [list job $prj(hex) [concat $cmd $objs]]
	unset cmd
	
	lappend cmd gpdasm -nos -p18f25k80 $prj(hex) | dd of=$prj(s) status=none
	depend $prj(s) [list $prj(hex)] [list job $prj(s) $cmd]
	unset cmd
	
	make $prj(s)
	
	debug "%s : %d bytes" $prj(hex) [file size $prj(hex)]
}

set sdcc_src sdcc-src-20211009-12697.tar.bz2
depend $dl/$sdcc_src {} {
	exec 2>@stderr >@stdout wget "http://sourceforge.net/projects/sdcc/files/snapshot_builds/sdcc-src/$sdcc_src" -O $::dl/$::sdcc_src
}
depend $dl/sdcc/.touch [list $dl/$sdcc_src] {
	exec 2>@stderr >@stdout tar xvj -C $::dl -f $::dl/$::sdcc_src
	touch $::dl/sdcc/.touch
}

set gputils_src gputils-1.5.0-1.tar.bz2
depend $dl/$gputils_src {} {
	exec 2>@stderr >@stdout wget "https://sourceforge.net/projects/gputils/files/gputils/1.5.0/$gputils_src" -O $::dl/$::gputils_src
}
depend $dl/gputils-1.5.0/.touch [list $dl/$gputils_src] {
	exec 2>@stderr >@stdout tar xvj -C $::dl -f $::dl/$::gputils_src
	touch $::dl/gputils-1.5.0/.touch
}
#sudo apt-get install bison flex texinfo

depend $dl/bin/gpasm [list $dl/gputils-1.5.0/.touch] {
	set p [pwd]
	cd $::dl/gputils-1.5.0
	exec 2>@stderr >@stdout ./configure --prefix=$::dl
	exec 2>@stderr >@stdout make -j 6
	exec 2>@stderr >@stdout make install
	cd $p
}

depend $dl/bin/sdcc [list $dl/sdcc/.touch $dl/bin/gpasm] {
	global env
	set p [pwd]
	cd $::dl/sdcc
	exec 2>@stderr >@stdout ./configure --prefix=$::dl
	exec 2>@stderr >@stdout make -j 6
	exec 2>@stderr >@stdout make install
	cd $p
}

make $dl/bin/sdcc

proc doc { file } {
	set path ""
	if { [exec hostname]!="TLa-home" } {
		append path /mnt/TLa-home
	}
	append path /data/documentations
	
	exec evince 2>/dev/null >/dev/null $path/$file &
}

if { $argv0==[info script] } {
	if { [info exists env(selection)] && $env(selection)!="" } {
		eval $env(selection)
	} {
	}
}


set # {
	doc electronics/microControleur/st/an3155.pdf
	doc electronics/microControleur/st/pm0075.pdf
	doc electronics/microControleur/st/stm32f103vb.pdf
	doc electronics/microControleur/st/stm32f10xxx-rm.pdf
}
